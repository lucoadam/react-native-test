import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import * as Font from "expo-font";
import EventDetail from "./App/EventDetail/EventDetail";
import Login from "./App/Login/Login";
import Messages from "./App/Messages/Messages";
import Profile from "./App/Profile/Profile";
import ProfilePhotos from "./App/ProfilePhotos/ProfilePhotos";
import ProfileSettings from "./App/ProfileSettings/ProfileSettings";

import Signup from "./App/Signup/Signup";
import Timeline from "./App/Timeline/Timeline";
import Welcome from "./App/Welcome/Welcome";

// import { AppLoading, DangerZone } from "expo";
// import { createAppContainer } from "@react-navigation/core";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
// import { createBottomTabNavigator } from "react-navigation";

const Stack = createStackNavigator();

function PushRouteOne() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Welcome" component={Welcome} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Signup" component={Signup} />
    </Stack.Navigator>
  );
}

function PushRouteTwo() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Timeline" component={Timeline} />
    </Stack.Navigator>
  );
}

function PushRouteThree() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Messages" component={Messages} />
    </Stack.Navigator>
  );
}

function PushRouteFour() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="ProfileSettings" component={ProfileSettings} />
      <Stack.Screen name="ProfilePhotos" component={ProfilePhotos} />
    </Stack.Navigator>
  );
}

function PushRouteFive() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="EventDetail" component={EventDetail} />
    </Stack.Navigator>
  );
}

const Tab = createBottomTabNavigator();

function Untitled() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarLabel: ({ color }) => {
          let text = "Home";
          if (route.name == "PushRouteTwo") {
            text = "Timeline";
          } else if (route.name == "PushRouteThree") {
            text = "Messages";
          } else if (route.name == "PushRouteFour") {
            text = "Profile";
          }
          return <Text style={{ color }}>{text}</Text>;
        },
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          console.log(route.name);
          if (route.name == "PushRouteTwo") {
            iconName = require("./assets/images/active-icon-7.png");
          } else if (route.name == "PushRouteThree") {
            iconName = require("./assets/images/active-icon-3.png");
          } else if (route.name == "PushRouteFour") {
            iconName = require("./assets/images/active-icon-6.png");
          }

          // You can return any component that you like here!
          return <Image source={iconName} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: "rgb(139, 27, 140)",
        inactiveTintColor: "rgb(139, 27, 140)",
        indicatorStyle: {
          backgroundColor: "transparent",
        },
        style: {
          backgroundColor: "rgb(248, 248, 248)",
        },
      }}
    >
      <Tab.Screen
        name="PushRouteTwo"
        label="Timeline"
        component={PushRouteTwo}
      />
      <Tab.Screen name="PushRouteThree" component={PushRouteThree} />
      <Tab.Screen name="PushRouteFour" component={PushRouteFour} />
    </Tab.Navigator>
  );
}

function RootNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="PushRouteOne" component={PushRouteOne} />
      <Stack.Screen name="Untitled" component={Untitled} />
      <Stack.Screen name="PushRouteFive" component={PushRouteFive} />
    </Stack.Navigator>
  );
}

// const RootNavigator = createStackNavigator(
//   {
//     PushRouteOne: {
//       screen: PushRouteOne,
//     },
//     // Untitled: {
//     //   screen: Untitled,
//     // },
//     // PushRouteFive: {
//     //   screen: PushRouteFive,
//     // },
//   },
//   {
//     mode: "modal",
//     headerMode: "none",
//     initialRouteName: "PushRouteOne",
//   }
// );

// const AppContainer = createAppContainer(RootNavigator);

async function initProjectFonts() {
  await Font.loadAsync({
    ".AppleSystemUIFont": require("./assets/fonts/SFNS.ttf"),
  });
}

export default function App() {
  initProjectFonts();
  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
